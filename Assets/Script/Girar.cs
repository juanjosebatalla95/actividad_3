using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girar : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotation;
    public float speed;

    // Update is called once per frame
    void Update()
    {
        rotation=Moverse.ClampVector3(rotation);
        transform.Rotate(rotation*(speed*Time.deltaTime));

    }
   


}

